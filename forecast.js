//https://api.openweathermap.org/data/2.5/forecast?q=London,us&units=metric&appid=085f5cc13314364a3103a85f3f67cf23

const key = "085f5cc13314364a3103a85f3f67cf23";
const getForecast = async(city) => {
	const base = "https://api.openweathermap.org/data/2.5/forecast";
	const query = `?q=${city}&units=metric&appid=${key}`;
	
	const response = await fetch(base+query);
	
	if(!response.ok)
		throw new Error("Status Code:" + response.status);
	
	const data = await response.json();
	return data;
}

//getForecast('Gujarat')
//.then(data=>console.log(data))
//.catch(err=>console.warn(err));